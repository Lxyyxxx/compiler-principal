#pragma once
/* 非终结符
S       0
B       2
B1 - H  3
C       5
C1 - J  6
D       8
D1 - L  9
D2 - M  10
E       11
E1 - N  12
*/

/* 终结符
Variable - a    0
Constant - b    1
log - d         3
sin - e         4
cos - f         5
tg - g          6
ctg - h         7
lg - i          8
ln - j          9
^               10
+               11
-               12
*               13
/               14
=               15
?               16
(               17
)               18
,               19
;               20
#               21
epsilon - @     22
*/

/* 语法树 */
struct TreeNode {
    char id;
    std::vector<TreeNode*> children;
    TreeNode(char id) { this->id = id, children.clear(); }
};

/* 语法分析器 */
class SyntacticAnalysis {
private:
    /* 预测分析表 */
    std::string predict_table[13][23];
    /* 记号流id对应的记号 */
    std::string token_table = " abcdefghij^+-*/=?(),;#";
    /* 语法树根节点*/
    TreeNode* root;

public:
    /* 是否是非终结符 */
    bool isN(char ch) {
        int a = GetIndexFromN(ch);
        if (a == -1) return false;
        return true;
    }

    /* 是否是终结符 */
    bool isT(char ch) {
        int a = GetIndexFromT(ch);
        if (a == -1) return false;
        return true;
    }

    /* 获得终结符id */
    int GetIndexFromT(char T) {
        switch (T) {
        case 'a':
            return 0;
        case 'b':
            return 1;
        case 'd':
            return 3;
        case 'e':
            return 4;
        case 'f':
            return 5;
        case 'g':
            return 6;
        case 'h':
            return 7;
        case 'i':
            return 8;
        case 'j':
            return 9;
        case '^':
            return 10;
        case '+':
            return 11;
        case '-':
            return 12;
        case '*':
            return 13;
        case '/':
            return 14;
        case '=':
            return 15;
        case '?':
            return 16;
        case '(':
            return 17;
        case ')':
            return 18;
        case ',':
            return 19;
        case ';':
            return 20;
        case '#':
            return 21;
        default:
            return -1;
        }
    }

    /* 获得非终结符id */
    int GetIndexFromN(char N) {
        switch (N) {
        case 'S':
            return 0;
        case 'B':
            return 2;
        case 'H':
            return 3;
        case 'C':
            return 5;
        case 'J':
            return 6;
        case 'D':
            return 8;
        case 'L':
            return 9;
        case 'M':
            return 10;
        case 'E':
            return 11;
        case 'N':
            return 12;
        default:
            return -1;
        }
    }

    /* 析构函数 */
    SyntacticAnalysis() {
        // 初始化预测分析表
        // S
        predict_table[0][0] = ";B=a";
        predict_table[0][16] = ";B?";
        predict_table[0][22] = "@";
        // B
        predict_table[2][0] = "HC";
        predict_table[2][1] = "HC";
        predict_table[2][3] = "HC";
        predict_table[2][4] = "HC";
        predict_table[2][5] = "HC";
        predict_table[2][6] = "HC";
        predict_table[2][7] = "HC";
        predict_table[2][8] = "HC";
        predict_table[2][9] = "HC";
        predict_table[2][11] = "HC";
        predict_table[2][12] = "HC";
        predict_table[2][17] = "HC";
        // B1 - H
        predict_table[3][11] = "HC+";
        predict_table[3][12] = "HC-";
        predict_table[3][18] = "@";
        predict_table[3][20] = "@";
        // C
        predict_table[5][0] = "JD";
        predict_table[5][1] = "JD";
        predict_table[5][3] = "JD";
        predict_table[5][4] = "JD";
        predict_table[5][5] = "JD";
        predict_table[5][6] = "JD";
        predict_table[5][7] = "JD";
        predict_table[5][8] = "JD";
        predict_table[5][9] = "JD";
        predict_table[5][13] = "JD";
        predict_table[5][14] = "JD";
        predict_table[5][17] = "JD";
        // C1 - J
        predict_table[6][13] = "JD*";
        predict_table[6][14] = "JD/";
        predict_table[6][11] = "@";
        predict_table[6][12] = "@";
        predict_table[6][18] = "@";
        predict_table[6][20] = "@";
        // D
        predict_table[8][0] = "ME";
        predict_table[8][1] = "ME";
        predict_table[8][4] = ")E(e";
        predict_table[8][5] = ")E(f";
        predict_table[8][6] = ")E(g";
        predict_table[8][7] = ")E(h";
        predict_table[8][8] = ")E(i";
        predict_table[8][9] = ")E(j";
        predict_table[8][11] = "ME";
        predict_table[8][12] = "ME";
        predict_table[8][17] = "ME";
        // D1 - L
        predict_table[9][18] = ")";
        predict_table[9][19] = ")E,";
        // D2 - M
        predict_table[10][10] = "E^";
        predict_table[10][11] = "@";
        predict_table[10][12] = "@";
        predict_table[10][13] = "@";
        predict_table[10][14] = "@";
        predict_table[10][18] = "@";
        predict_table[10][20] = "@";
        // E
        predict_table[11][0] = "N";
        predict_table[11][1] = "N";
        predict_table[11][11] = "N+";
        predict_table[11][12] = "N-";
        predict_table[11][17] = ")B(";
        // E1 - N
        predict_table[12][0] = "a";
        predict_table[12][1] = "b";
    }

    /* 查表 */
    std::string GetRight(char N, char T) {
        int row = GetIndexFromN(N);
        int col = GetIndexFromT(T);
        if (row == -1 || col == -1) return "";
        return predict_table[row][col];
    }

    /* 语法分析 */
    bool check(std::vector<psi> la_stream, std::wstring& outputString) {
        // 在串尾添加结束符#
        la_stream.push_back({ "#", 22 });
        const int N = la_stream.size();
        // 定义栈，并初始化
        std::stack<char> stk;
        stk.push('#');
        stk.push('S');
        // 初始化语法分析树
        root = new TreeNode('S');
        std::stack<TreeNode*> stk_tree;
        stk_tree.push(new TreeNode('#'));
        stk_tree.push(root);
        // 开始
        int cur_stream = 0;
        char cur_ch;
        std::string nxt;
        TreeNode* t_node;
        while (!stk.empty() && cur_stream < N) {
            cur_ch = stk.top(), stk.pop();
            t_node = stk_tree.top(), stk_tree.pop();
            // 非终结符
            if (isN(cur_ch)) {
                // 查表
                nxt =
                    GetRight(cur_ch, token_table[la_stream[cur_stream].second]);
                if (nxt.empty()) {
                    // 输出之前的单词及错误信息
                    for (int i = 0; i <= cur_stream; i++)
                        outputString += std::wstring(la_stream[i].first.begin(), la_stream[i].first.end());
                    outputString += L"\r\n在此处出现语法错误1\r\n";
                    outputString += std::wstring(1, cur_ch);
                    outputString += L"和";
                    outputString += std::wstring(1, token_table[la_stream[cur_stream].second]);
                    outputString += L"查表失败\r\n";
                    return false;
                }
                else {
                    // 如果是空串，直接跳过
                    if (nxt == "@") continue;
                    // 入栈并添加到语法树中
                    for (auto i : nxt) {
                        TreeNode* t = new TreeNode(i);
                        t_node->children.insert(t_node->children.begin(), t);
                        stk.push(i);
                        stk_tree.push(t);
                    }
                }
            }
            // 终结符
            else if (isT(cur_ch)) {
                // match
                if (cur_ch == token_table[la_stream[cur_stream].second]) {
                    outputString += L"Match：";
                    outputString += std::wstring(la_stream[cur_stream].first.begin(), la_stream[cur_stream].first.end());
                    outputString += L"\r\n";
                    cur_stream++;
                }
                else {
                    // 输出之前的单词及错误信息
                    for (int i = 0; i <= cur_stream; i++)
                        outputString += std::wstring(la_stream[i].first.begin(), la_stream[i].first.end());
                    outputString += L"\r\n在此处出现语法错误2\r\n";
                    outputString += L"终结符";
                    outputString += std::wstring(1, cur_ch);
                    outputString += L"匹配失败\r\n";
                    return false;
                }
            }
            else {
                // 输出之前的单词及错误信息
                for (int i = 0; i <= cur_stream; i++)
                    outputString += std::wstring(la_stream[i].first.begin(), la_stream[i].first.end());
                outputString += L"\r\n在此处出现语法错误3\r\n";
                outputString += std::wstring(1, cur_ch);
                outputString += L"既不是终结符也不是非终结符\r\n";
                return false;
            }
        }
        return true;
    }

    /* 先序遍历树 */
    void dfs(TreeNode* root, int level, std::wstring& outputString) {
        if (!root) return;
        for (int i = 0; i < level; i++) outputString += L" ";
        outputString += std::wstring(1, root->id);
        outputString += L"\r\n";
        for (auto i : root->children) dfs(i, level + 1, outputString);
    }

    /* 打印语法分析树 */
    void print(std::wstring& outputString) {
        TreeNode* t = root;
        dfs(t, 0, outputString);
    }
};