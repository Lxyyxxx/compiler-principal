#pragma once
/* 记号表
Variable - 1
Constant - 2
log - 4
sin - 5
cos - 6
tg - 7
ctg - 8
lg - 9
ln - 10
^ - 11
+ - 12
- - 13
* - 14
/ - 15
= - 16
? - 17
( - 18
) - 19
, - 20
; - 21
# - 22
*/

class LexicalAnalysis {
private:
    /* 记号流 */
    std::vector<psi> stream;

public:
    /* 状态转移函数 */
    int move(char ch, int state) {
        switch (state) {
        case 0:
            if (ch == '-' || ch == '+')
                return 1;
            else if (ch == '*' || ch == '/' || ch == '^' || ch == '=')
                return 8;
            else if (ch == ';' || ch == '(' || ch == ')' || ch == ' ' ||
                ch == ',' || ch == '?')
                return 9;
            else if (ch == '\\')
                return 18;
            else if (ch == 'P')
                return 2;
            else if (ch == 'E')
                return 4;
            else if (ch == '0')
                return 5;
            else if (ch >= '1' && ch <= '9')
                return 6;
            else if (ch == 's')
                return 10;
            else if (ch == 'c')
                return 11;
            else if (ch == 't')
                return 12;
            else if (ch == 'l')
                return 13;
            else if (ch == '_' || isalpha(ch) && ch != 'P' && ch != 'E' &&
                ch != 's' && ch != 'c' && ch != 't' &&
                ch != 'l')
                return 17;
            else
                return 19;
            break;
        case 1:
            return 19;
            break;
        case 2:
            if (ch == 'I')
                return 4;
            else if (ch == '_' || isdigit(ch) || isalpha(ch) && ch != 'I')
                return 17;
            else
                return 19;
            break;
        case 3:
            if (ch == '0')
                return 3;
            else if (ch >= '1' && ch <= '9')
                return 7;
            else
                return 19;
            break;
        case 4:
            if (ch == '_' || isdigit(ch) || isalpha(ch))
                return 17;
            else
                return 19;
            break;
        case 5:
            if (ch == '.')
                return 3;
            else
                return 19;
            break;
        case 6:
            if (ch >= '0' && ch <= '9')
                return 6;
            else if (ch == '.')
                return 3;
            else
                return 19;
            break;
        case 7:
            if (ch == '0')
                return 3;
            else if (ch >= '1' && ch <= '9')
                return 7;
            else
                return 19;
            break;
        case 8:
            return 19;
            break;
        case 9:
            return 19;
            break;
        case 10:
            if (ch == 'i')
                return 14;
            else if (ch == '_' || isdigit(ch) || isalpha(ch) && ch != 'i')
                return 17;
            else
                return 19;
            break;
        case 11:
            if (ch == 't')
                return 12;
            else if (ch == 'o')
                return 15;
            else if (ch == '_' || isdigit(ch) ||
                isalpha(ch) && ch != 't' && ch != 'o')
                return 17;
            else
                return 19;
            break;
        case 12:
            if (ch == 'g')
                return 16;
            else if (ch == '_' || isdigit(ch) || isalpha(ch) && ch != 'g')
                return 17;
            else
                return 19;
            break;
        case 13:
            if (ch == 'o')
                return 12;
            else if (ch == 'g' || ch == 'n')
                return 16;
            else if (ch == '_' || isdigit(ch) ||
                isalpha(ch) && ch != 'g' && ch != 'o' && ch != 'n')
                return 17;
            else
                return 19;
            break;
        case 14:
            if (ch == 'n')
                return 16;
            else if (ch == '_' || isdigit(ch) || isalpha(ch) && ch != 'n')
                return 17;
            else
                return 19;
            break;
        case 15:
            if (ch == 's')
                return 16;
            else if (ch == '_' || isdigit(ch) || isalpha(ch) && ch != 's')
                return 17;
            else
                return 19;
            break;
        case 16:
            if (ch == '_' || isdigit(ch) || isalpha(ch))
                return 17;
            else
                return 19;
            break;
        case 17:
            if (ch == '_' || isdigit(ch) || isalpha(ch))
                return 17;
            else
                return 19;
            break;
        case 18:
            if (ch == 'n' || ch == 't')
                return 9;
            else
                return 19;
            break;
        case 19:
            return 19;
            break;
        default:
            return 19;
        }
    }

    /* 是否为常量 */
    bool isConstant(int state) {
        return (state == 4 || state == 5 || state == 6 || state == 7);
    }

    /* 是否为变量 */
    bool isVariable(int state) {
        return state == 17 || state == 2 || state == 4 || state == 10 ||
            state == 11 || state == 12 || state == 13;
    }

    /* 是否为关键字 */
    bool isKeyword(int state) { return state == 16; }

    /* 是否为运算符 */
    bool isOperation(int state) { return (state == 1 || state == 8); }

    /* 是否为分隔符 */
    bool isDelimiter(int state) { return state == 9; }

    /* 判断是否为有效的串，如果有效输出它的类型 */
    bool print(int state, std::string result, std::wstring& outputString) {
        bool flag = false;
        if (isConstant(state)) {
            stream.push_back({ result, 2 });
            flag = true;
        }
        else if (isVariable(state)) {
            // 字符不超过32个
            if (result.size() > 32) {
                outputString += L"词法错误\r\n";
                outputString += std::wstring(result.begin(), result.end());
                outputString += L"\r\n变量长度超过了32个字符\r\n";
                stream.clear();
            }
            else {
                stream.push_back({ result, 1 });
                flag = true;
            }
        }
        else if (isKeyword(state)) {
            if (result == "log")
                stream.push_back({ result, 4 });
            else if (result == "sin")
                stream.push_back({ result, 5 });
            else if (result == "cos")
                stream.push_back({ result, 6 });
            else if (result == "tg")
                stream.push_back({ result, 7 });
            else if (result == "ctg")
                stream.push_back({ result, 8 });
            else if (result == "lg")
                stream.push_back({ result, 9 });
            else if (result == "ln")
                stream.push_back({ result, 10 });
            flag = true;
        }
        else if (isOperation(state)) {
            if (result == "^")
                stream.push_back({ result, 11 });
            else if (result == "+")
                stream.push_back({ result, 12 });
            else if (result == "-")
                stream.push_back({ result, 13 });
            else if (result == "*")
                stream.push_back({ result, 14 });
            else if (result == "/")
                stream.push_back({ result, 15 });
            else if (result == "=")
                stream.push_back({ result, 16 });
            flag = true;
        }
        else if (isDelimiter(state)) {
            if (result == "?")
                stream.push_back({ result, 17 });
            else if (result == "(")
                stream.push_back({ result, 18 });
            else if (result == ")")
                stream.push_back({ result, 19 });
            else if (result == ",")
                stream.push_back({ result, 20 });
            else if (result == ";")
                stream.push_back({ result, 21 });
            // 如果是分隔符，直接过滤
            flag = true;
        }
        return flag;
    }

    /* 遍历串，并检测是否均为有效串 */
    bool valid(std::string str, std::wstring& outputString) {
        int pre_state = 0, now_state = 19;
        std::string res;
        stream.clear();
        for (char s : str) {
            now_state = move(s, pre_state);
            if (now_state == 19) {
                if (!print(pre_state, res, outputString)) return false;
                res.clear();
                pre_state = 0;
                now_state = move(s, pre_state);
            }
            res += s;
            pre_state = now_state;
        }
        if (now_state == 19 && !print(pre_state, res, outputString) || !print(now_state, res, outputString))
            return false;
        return true;
    }

    /* 得到词法分析结果 */
    std::vector<psi> get_result() { return stream; }
};