#pragma once
/* SLR table
    Var	Con	Key	+	-	*	/	^	(	)	,	;	=	?	log	#	S	B	C	D	E
0	s2													s3			1
1																acc
2													s4
3	s13	s14	s10	s11	s12				s15						s8			5	6	7	9
4	s13	s14	s10	s11	s12				s15						s8			16	6	7	9
5				s17	s18							s43
6				r6	r6	s19	s20			r6		r6
7				r9	r9	r9	r9			r9		r9
8									s21
9				r14	r14	r14	r14	s22		r14		r14
10									s23
11	s24	s25
12	s26	s27
13				r19	r19	r19	r19	r19		r19		r19
14				r20	r20	r20	r20	r20		r20		r20
15	s13	s14	s10	s11	s12				s15						s8			28	6	7	9
16				s17	s18							s29
17	s13	s14	s10	s11	s12				s15						s8				30	7	9
18	s13	s14	s10	s11	s12				s15						s8				31	7	9
19	s13	s14	s10	s11	s12				s15						s8					32	9
20	s13	s14	s10	s11	s12				s15						s8					33	9
21	s13	s14		s11	s12				s15												34
22	s13	s14		s11	s12				s15												35
23	s13	s14		s11	s12				s15												36
24				r15	r15	r15	r15	r15		r15		r15
25				r17	r17	r17	r17	r17		r17		r17
26				r16	r16	r16	r16	r16		r16		r16
27				r18	r18	r18	r18	r18		r18		r18
28				s17	s18					s37
29																r2
30				r4	r4	s19	s20			r4		r4
31				r5	r5	s19	s20			r5		r5
32				r7	r7	r7	r7			r7		r7
33				r8	r8	r8	r8			r8		r8
34										s39	s38
35				r12	r12	r12	r12			r12		r12
36										s40
37				r21	r21	r21	r21	r21		r21		r21
38	s13	s14		s11	s12				s15												41
39				r11	r11	r11	r11			r11		r11
40				r13	r13	r13	r13			r13		r13
41										s42
42				r10	r10	r10	r10			r10		r10
43																r3
*/

/* SLR(1)
1	S’ -> S
2	S -> Variable=B;
3	S -> ?B;
4	B -> B+C
5	B -> B-C
6	B -> C
7	C -> C*D
8	C -> C/D
9	C -> D
10	D -> log(E,E)
11	D -> log(E)
12	D -> E^E
13	D -> Keyword(E)
14	D -> E
15	E -> +Variable
16	E -> -Variable
17	E -> +Constant
18	E -> -Constant
19	E -> Variable
20	E -> Constant
21	E -> (B)
*/

/* 终结符
Variable - a    0
Constant - b    1
log - d         3
sin - e         4
cos - f         5
tg - g          6
ctg - h         7
lg - i          8
ln - j          9
^               10
+               11
-               12
*               13
/               14
=               15
?               16
(               17
)               18
,               19
;               20
#               21
*/

/* 非终结符
S'      0
S       1
B       2
C       3
D       4
E       5
*/

// string to wstring
std::wstring s2w(const std::string& str) {
    int num = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, NULL, 0);
    wchar_t* wide = new wchar_t[num];
    MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, wide, num);
    std::wstring w_str(wide);
    delete[] wide;
    return w_str;
}

/* 语法树 */
struct TreeNode {
    // 表示终结符或非终结符
    char id;
    // 表示计算的值
    double val;
    // 表示变量
    std::string variable;
    // 表示函数或运算符
    std::string fx;
    // 1 为常量， 0 为变量， 2为符号
    unsigned int flag;

    std::vector<TreeNode*> children;
    TreeNode* parent;
    TreeNode(char id) { this->id = id, children.clear(), parent = nullptr, this->val = -1, this->variable = this->fx = "-1", this->flag = 2; }
    TreeNode(char id, double val) { this->id = id, children.clear(), parent = nullptr, this->val = val, this->variable = this->fx = "-1", this->flag = 1; }
    TreeNode(char id, std::string v) { this->id = id, children.clear(), parent = nullptr, this->val = -1, this->variable = v, this->fx = "-1", this->flag = 0; }
};

/* 语义分析器 */
class SemanticAnalysis {
private:
    /* 记号流id对应的记号 */
    std::string token_table = " abcdefghij^+-*/=?(),;#";
    /* 语法树根节点*/
    TreeNode* root = nullptr;
    /* 产生式左部 */
    char GramLeft[22];
    /* 产生式右部个数 */
    int GramNum[22];
    /* 移进归约表 */
    int SLR[44][22];
    /* Goto表 */
    int Goto[44][6];
    /* 变量的值 */
    std::unordered_map<std::string, double>Symbol;
    /* 终结符， 符号转名字 */
    std::unordered_map<char, std::string>end_symbol;
    /* 式子是赋值还是计算 0-赋值 1-计算*/
    bool ff;

public:
    /* 析构函数 */
    SemanticAnalysis() {
        // 产生式左部
        GramLeft[2] = 'S';
        GramLeft[3] = 'S';
        GramLeft[4] = 'B';
        GramLeft[5] = 'B';
        GramLeft[6] = 'B';
        GramLeft[7] = 'C';
        GramLeft[8] = 'C';
        GramLeft[9] = 'C';
        GramLeft[10] = 'D';
        GramLeft[11] = 'D';
        GramLeft[12] = 'D';
        GramLeft[13] = 'D';
        GramLeft[14] = 'D';
        GramLeft[15] = 'E';
        GramLeft[16] = 'E';
        GramLeft[17] = 'E';
        GramLeft[18] = 'E';
        GramLeft[19] = 'E';
        GramLeft[20] = 'E';
        GramLeft[21] = 'E';

        // 产生式右部个数
        GramNum[2] = 4;
        GramNum[3] = 3;
        GramNum[4] = 3;
        GramNum[5] = 3;
        GramNum[6] = 1;
        GramNum[7] = 3;
        GramNum[8] = 3;
        GramNum[9] = 1;
        GramNum[10] = 6;
        GramNum[11] = 4;
        GramNum[12] = 3;
        GramNum[13] = 4;
        GramNum[14] = 1;
        GramNum[15] = 2;
        GramNum[16] = 2;
        GramNum[17] = 2;
        GramNum[18] = 2;
        GramNum[19] = 1;
        GramNum[20] = 1;
        GramNum[21] = 3;

        // 移进归约表
        // s - +, r - -, acc - 44
        SLR[0][0] = 2;
        SLR[0][16] = 3;
        SLR[1][21] = 44;
        SLR[2][15] = 4;
        SLR[3][0] = 13;
        SLR[3][1] = 14;
        SLR[3][4] = 10;
        SLR[3][5] = 10;
        SLR[3][6] = 10;
        SLR[3][7] = 10;
        SLR[3][8] = 10;
        SLR[3][9] = 10;
        SLR[3][11] = 11;
        SLR[3][12] = 12;
        SLR[3][17] = 15;
        SLR[3][3] = 8;
        SLR[4][0] = 13;
        SLR[4][1] = 14;
        SLR[4][4] = 10;
        SLR[4][5] = 10;
        SLR[4][6] = 10;
        SLR[4][7] = 10;
        SLR[4][8] = 10;
        SLR[4][9] = 10;
        SLR[4][11] = 11;
        SLR[4][12] = 12;
        SLR[4][17] = 15;
        SLR[4][3] = 8;
        SLR[5][11] = 17;
        SLR[5][12] = 18;
        SLR[5][20] = 43;
        SLR[6][11] = -6;
        SLR[6][12] = -6;
        SLR[6][13] = 19;
        SLR[6][14] = 20;
        SLR[6][18] = -6;
        SLR[6][20] = -6;
        SLR[7][11] = -9;
        SLR[7][12] = -9;
        SLR[7][13] = -9;
        SLR[7][14] = -9;
        SLR[7][18] = -9;
        SLR[7][20] = -9;
        SLR[8][17] = 21;
        SLR[9][11] = -14;
        SLR[9][12] = -14;
        SLR[9][13] = -14;
        SLR[9][14] = -14;
        SLR[9][10] = 22;
        SLR[9][18] = -14;
        SLR[9][20] = -14;
        SLR[10][17] = 23;
        SLR[11][0] = 24;
        SLR[11][1] = 25;
        SLR[12][0] = 26;
        SLR[12][1] = 27;
        SLR[13][10] = -19;
        SLR[13][11] = -19;
        SLR[13][12] = -19;
        SLR[13][13] = -19;
        SLR[13][14] = -19;
        SLR[13][18] = -19;
        SLR[13][20] = -19;
        SLR[14][10] = -20;
        SLR[14][11] = -20;
        SLR[14][12] = -20;
        SLR[14][13] = -20;
        SLR[14][14] = -20;
        SLR[14][18] = -20;
        SLR[14][20] = -20;
        SLR[15][0] = 13;
        SLR[15][1] = 14;
        SLR[15][4] = 10;
        SLR[15][5] = 10;
        SLR[15][6] = 10;
        SLR[15][7] = 10;
        SLR[15][8] = 10;
        SLR[15][9] = 10;
        SLR[15][11] = 11;
        SLR[15][12] = 12;
        SLR[15][17] = 15;
        SLR[15][3] = 8;
        SLR[16][11] = 17;
        SLR[16][12] = 18;
        SLR[16][20] = 29;
        SLR[17][0] = 13;
        SLR[17][1] = 14;
        SLR[17][4] = 10;
        SLR[17][5] = 10;
        SLR[17][6] = 10;
        SLR[17][7] = 10;
        SLR[17][8] = 10;
        SLR[17][9] = 10;
        SLR[17][11] = 11;
        SLR[17][12] = 12;
        SLR[17][17] = 15;
        SLR[17][3] = 8;
        SLR[18][0] = 13;
        SLR[18][1] = 14;
        SLR[18][4] = 10;
        SLR[18][5] = 10;
        SLR[18][6] = 10;
        SLR[18][7] = 10;
        SLR[18][8] = 10;
        SLR[18][9] = 10;
        SLR[18][11] = 11;
        SLR[18][12] = 12;
        SLR[18][17] = 15;
        SLR[18][3] = 8;
        SLR[19][0] = 13;
        SLR[19][1] = 14;
        SLR[19][4] = 10;
        SLR[19][5] = 10;
        SLR[19][6] = 10;
        SLR[19][7] = 10;
        SLR[19][8] = 10;
        SLR[19][9] = 10;
        SLR[19][11] = 11;
        SLR[19][12] = 12;
        SLR[19][17] = 15;
        SLR[19][3] = 8;
        SLR[20][0] = 13;
        SLR[20][1] = 14;
        SLR[20][4] = 10;
        SLR[20][5] = 10;
        SLR[20][6] = 10;
        SLR[20][7] = 10;
        SLR[20][8] = 10;
        SLR[20][9] = 10;
        SLR[20][11] = 11;
        SLR[20][12] = 12;
        SLR[20][17] = 15;
        SLR[20][3] = 8;
        SLR[21][0] = 13;
        SLR[21][1] = 14;
        SLR[21][11] = 11;
        SLR[21][12] = 12;
        SLR[21][17] = 15;
        SLR[22][0] = 13;
        SLR[22][1] = 14;
        SLR[22][11] = 11;
        SLR[22][12] = 12;
        SLR[22][17] = 15;
        SLR[23][0] = 13;
        SLR[23][1] = 14;
        SLR[23][11] = 11;
        SLR[23][12] = 12;
        SLR[23][17] = 15;
        SLR[24][10] = -15;
        SLR[24][11] = -15;
        SLR[24][12] = -15;
        SLR[24][13] = -15;
        SLR[24][14] = -15;
        SLR[24][18] = -15;
        SLR[24][20] = -15;
        SLR[25][10] = -17;
        SLR[25][11] = -17;
        SLR[25][12] = -17;
        SLR[25][13] = -17;
        SLR[25][14] = -17;
        SLR[25][18] = -17;
        SLR[25][20] = -17;
        SLR[26][10] = -16;
        SLR[26][11] = -16;
        SLR[26][12] = -16;
        SLR[26][13] = -16;
        SLR[26][14] = -16;
        SLR[26][18] = -16;
        SLR[26][20] = -16;
        SLR[27][10] = -18;
        SLR[27][11] = -18;
        SLR[27][12] = -18;
        SLR[27][13] = -18;
        SLR[27][14] = -18;
        SLR[27][18] = -18;
        SLR[27][20] = -18;
        SLR[28][11] = 17;
        SLR[28][12] = 18;
        SLR[28][18] = 37;
        SLR[29][21] = -2;
        SLR[30][11] = -4;
        SLR[30][12] = -4;
        SLR[30][13] = 19;
        SLR[30][14] = 20;
        SLR[30][18] = -4;
        SLR[30][20] = -4;
        SLR[31][11] = -5;
        SLR[31][12] = -5;
        SLR[31][13] = 19;
        SLR[31][14] = 20;
        SLR[31][18] = -5;
        SLR[31][20] = -5;
        SLR[32][11] = -7;
        SLR[32][12] = -7;
        SLR[32][13] = -7;
        SLR[32][14] = -7;
        SLR[32][18] = -7;
        SLR[32][20] = -7;
        SLR[33][11] = -8;
        SLR[33][12] = -8;
        SLR[33][13] = -8;
        SLR[33][14] = -8;
        SLR[33][18] = -8;
        SLR[33][20] = -8;
        SLR[34][18] = 39;
        SLR[34][19] = 38;
        SLR[35][11] = -12;
        SLR[35][12] = -12;
        SLR[35][13] = -12;
        SLR[35][14] = -12;
        SLR[35][18] = -12;
        SLR[35][20] = -12;
        SLR[36][18] = 40;
        SLR[37][10] = -21;
        SLR[37][11] = -21;
        SLR[37][12] = -21;
        SLR[37][13] = -21;
        SLR[37][14] = -21;
        SLR[37][18] = -21;
        SLR[37][20] = -21;
        SLR[38][0] = 13;
        SLR[38][1] = 14;
        SLR[38][11] = 11;
        SLR[38][12] = 12;
        SLR[38][17] = 15;
        SLR[39][11] = -11;
        SLR[39][12] = -11;
        SLR[39][13] = -11;
        SLR[39][14] = -11;
        SLR[39][18] = -11;
        SLR[39][20] = -11;
        SLR[40][11] = -13;
        SLR[40][12] = -13;
        SLR[40][13] = -13;
        SLR[40][14] = -13;
        SLR[40][18] = -13;
        SLR[40][20] = -13;
        SLR[41][18] = 42;
        SLR[42][11] = -10;
        SLR[42][12] = -10;
        SLR[42][13] = -10;
        SLR[42][14] = -10;
        SLR[42][18] = -10;
        SLR[42][20] = -10;
        SLR[43][21] = -3;

        // Goto表
        Goto[0][1] = 1;
        Goto[3][2] = 5;
        Goto[3][3] = 6;
        Goto[3][4] = 7;
        Goto[3][5] = 9;
        Goto[4][2] = 16;
        Goto[4][3] = 6;
        Goto[4][4] = 7;
        Goto[4][5] = 9;
        Goto[15][2] = 28;
        Goto[15][3] = 6;
        Goto[15][4] = 7;
        Goto[15][5] = 9;
        Goto[17][3] = 30;
        Goto[17][4] = 7;
        Goto[17][5] = 9;
        Goto[18][3] = 31;
        Goto[18][4] = 7;
        Goto[18][5] = 9;
        Goto[19][4] = 32;
        Goto[19][5] = 9;
        Goto[20][4] = 33;
        Goto[20][5] = 9;
        Goto[21][5] = 34;
        Goto[22][5] = 35;
        Goto[23][5] = 36;
        Goto[38][5] = 41;

        // 符号表添加两个常量的值
        Symbol["PI"] = acos(-1);
        Symbol["E"] = exp(1);
    }

    /* 获得终结符id */
    int GetIndexFromT(char T) {
        switch (T) {
        case 'a':
            return 0;
        case 'b':
            return 1;
        case 'd':
            return 3;
        case 'e':
            return 4;
        case 'f':
            return 5;
        case 'g':
            return 6;
        case 'h':
            return 7;
        case 'i':
            return 8;
        case 'j':
            return 9;
        case '^':
            return 10;
        case '+':
            return 11;
        case '-':
            return 12;
        case '*':
            return 13;
        case '/':
            return 14;
        case '=':
            return 15;
        case '?':
            return 16;
        case '(':
            return 17;
        case ')':
            return 18;
        case ',':
            return 19;
        case ';':
            return 20;
        case '#':
            return 21;
        default:
            return -1;
        }
    }

    /* 获得非终结符id */
    int GetIndexFromN(char N) {
        switch (N) {
        case 'S':
            return 1;
        case 'B':
            return 2;
        case 'C':
            return 3;
        case 'D':
            return 4;
        case 'E':
            return 5;
        default:
            return -1;
        }
    }
    
    /* 终结符id转名字 */
    std::string GetNameFromT(char T) {
        switch (T) {
        case 'd':
            return "log";
        case 'e':
            return "sin";
        case 'f':
            return "cos";
        case 'g':
            return "tg";
        case 'h':
            return "ctg";
        case 'i':
            return "lg";
        case 'j':
            return "ln";
        default:
            return std::string(1, T);
        }
    }

    /* 删除孩子 */
    void delete_children(TreeNode* root) {
        for (auto& i : root->children)
            delete i;
        root->children.clear();
    }
    
    /* 计算表达式规约 */
    void calculate(TreeNode* root, int reduce_id, std::wstring& outputString) {
        // S -> Variable=B;
        if (reduce_id == 2) {
            this->ff = 0;
            // 是常量才赋值
            if (root->children[2]->flag == 1) {
                Symbol[root->children[0]->variable] = root->children[2]->val;
            }
        }
        // S -> ?B;
        else if (reduce_id == 3) {
            this->root = root->children[1];
            this->ff = 1;
        }
        // B -> B+C
        else if (reduce_id == 4 && root->children[0]->flag == 1 && root->children[2]->flag == 1) {
            // 都有值才能计算
            root->val = root->children[0]->val + root->children[2]->val;
            root->flag = 1;
            // 删除孩子
            delete_children(root);
        }
        // B -> B-C
        else if (reduce_id == 5 && root->children[0]->flag == 1 && root->children[2]->flag == 1) {
            root->val = root->children[0]->val - root->children[2]->val;
            root->flag = 1;
            delete_children(root);
        }
        // B -> C | C -> D | D -> E
        else if ((reduce_id == 6 || reduce_id == 9 || reduce_id == 14) && root->children[0]->flag == 1) {
            // 直接等于孩子，并删除孩子
            if (root->children[0]->flag == 1) {
                root->val = root->children[0]->val;
                root->flag = 1;
                delete_children(root);
            }
        }
        // C -> C*D
        else if (reduce_id == 7 && root->children[0]->flag == 1 && root->children[2]->flag == 1) {
            root->val = root->children[0]->val * root->children[2]->val;
            root->flag = 1;
            delete_children(root);
        }
        // C -> C/D
        else if (reduce_id == 8 && root->children[0]->flag == 1 && root->children[2]->flag == 1 && root->children[2]->val != 0) {
            root->val = root->children[0]->val / root->children[2]->val;
            root->flag = 1;
            delete_children(root);
        }
        // D -> log(E,E)
        else if (reduce_id == 10 && root->children[2]->flag == 1 && root->children[4]->flag == 1 && root->children[2]->val > 0 && root->children[4]->val > 0) {
            root->val = log(root->children[4]->val) / log(root->children[2]->val); 
            root->flag = 1;
            root->children.clear();
        }
        // D -> log(E)
        else if (reduce_id == 11 && root->children[2]->flag == 1 && root->children[2]->val > 0) {
            root->val = log2(root->children[2]->val);
            root->flag = 1;
            delete_children(root);
        }
        // D -> E^E
        else if (reduce_id == 12 && root->children[0]->flag == 1 && root->children[2]->flag == 1) {
            root->val = pow(root->children[0]->val, root->children[2]->val);
            root->flag = 1;
            delete_children(root);
        }
        // D -> Keyword(E)
        else if (reduce_id == 13 && root->children[2]->flag == 1) {
            // sin
            if (root->children[0]->fx == "sin")
                root->val = sin(root->children[2]->val);
            // cos
            else if (root->children[0]->fx == "cos")
                root->val = cos(root->children[2]->val);
            // tg
            else if (root->children[0]->fx == "tg" && cos(root->children[2]->val) != 0)
                root->val = tan(root->children[2]->val);
            // ctg
            else if (root->children[0]->fx == "ctg" && sin(root->children[2]->val) != 0)
                root->val = 1 / tan(root->children[2]->val);
            // lg
            else if (root->children[0]->fx == "lg" && root->children[2]->val > 0)
                root->val = log10(root->children[2]->val);
            // ln
            else if (root->children[0]->fx == "ln" && root->children[2]->val > 0)
                root->val = log(root->children[2]->val);
            root->flag = 1;
            delete_children(root);
        }
        // E -> +Variable | E -> +Constant
        else if ((reduce_id == 15 || reduce_id == 17) && root->children[1]->flag == 1) {
            root->val = root->children[1]->val;
            root->flag = 1;
            delete_children(root);
        }
        // E -> -Variable | E -> -Constant
        else if ((reduce_id == 16 || reduce_id == 18) && root->children[1]->flag == 1) {
            root->val = -root->children[1]->val;
            root->flag = 1;
            delete_children(root);
        }
        // E -> Variable | E-> Constant
        else if ((reduce_id == 19 || reduce_id == 20) && root->children[0]->flag == 1) {
            root->val = root->children[0]->val;
            root->flag = 1;
            delete_children(root);
        }
        // E -> (B)
        else if (reduce_id == 21 && root->children[1]->flag == 1) {
            root->val = root->children[1]->val;
            root->flag = 1;
            delete_children(root);
        }
    }

    /* 语义分析 */
    bool check(std::vector<psi> la_stream, std::wstring& outputString) {
        // 在串尾添加结束符#
        la_stream.push_back({ "#", 22 });
        // 记号流长度
        const int N = la_stream.size();
        // 符号栈
        std::stack<char> symbol_stk;
        symbol_stk.push('#');
        // 状态栈
        std::stack<int> state_stk;
        state_stk.push(0);
        // 当前的字符
        int cur = 0;
        // 表的行列
        int row, col;
        // 动作，查表获得
        int action;
        // goto动作，查表获得
        int goTo;
        // 文法右部的个数
        int times;
        // 文法左部
        char left;
        // 存没有parent的树结点
        std::stack<TreeNode*>v;
        // 规约的产生式编号
        int reduce_id;
        // 当前子树的parent和没有parent的top
        TreeNode* parent = nullptr, * top = nullptr;
        // reduce弹出的记号
        char reduce_ch;
        // 记号流栈
        std::stack<std::string>value_stk;
        value_stk.push("#");
        // 规约的记号
        std::string reduce_val;
        while (cur < N) {
            // 根据栈顶状态和当前输入进行查表
            row = state_stk.top();
            col = GetIndexFromT(token_table[la_stream[cur].second]);
            action = SLR[row][col];
            // accept
            if (action == 44) {
                // 如果是赋值操作，将root设为parent；否则根不变
                if (!this->ff)root = v.top();
                v.pop();
                return true;
            }
            // shift
            else if (action > 0) {
                symbol_stk.push(token_table[la_stream[cur].second]);
                state_stk.push(action);
                value_stk.push(la_stream[cur].first);
                cur++;
            }
            // reduce
            else if (action < 0) {
                reduce_id = -action;
                left = GramLeft[reduce_id];
                // 父亲节点为产生式左部
                parent = new TreeNode(left);
                // 1.找到文法。从栈中弹出文法的右部，并加入到语法树中
                times = GramNum[reduce_id];
                while (times--) {
                    reduce_ch = symbol_stk.top();
                    symbol_stk.pop();
                    state_stk.pop();
                    reduce_val = value_stk.top();
                    value_stk.pop();
                    // 如果节点已生成，直接设为孩子，并设parent为父亲
                    if (!v.empty() && v.top()->id == reduce_ch) {
                        top = v.top();
                        v.pop();
                        parent->children.insert(parent->children.begin(), top);
                        top->parent = parent;
                    }
                    // 否则生成新节点
                    else {
                        // 变量
                        if (reduce_ch == 'a') {
                            // 如果变量存在则存入值
                            if (Symbol.count(reduce_val))
                                top = new TreeNode(reduce_ch, Symbol[reduce_val]);
                            // 否则存入变量名
                            else
                                top = new TreeNode(reduce_ch, reduce_val);
                        }
                        // 常量
                        else if (reduce_ch == 'b') {
                            // 如果示PI或E就转为值
                            if (reduce_val == "PI" || reduce_val == "E")
                                top = new TreeNode(reduce_ch, Symbol[reduce_val]);
                            else
                                top = new TreeNode(reduce_ch, stod(reduce_val));
                        }
                        // 函数或符号
                        else {
                            top = new TreeNode(reduce_ch);
                            top->fx = GetNameFromT(reduce_ch);
                        }
                        parent->children.insert(parent->children.begin(), top);
                        top->parent = parent;
                    }
                }
                // 表达式规约
                calculate(parent, reduce_id, outputString);
                // top置为parent，并存入v中
                top = parent;
                v.push(top);
                // 2.把左部符号压栈，查Goto表，把查到的状态压栈
                row = state_stk.top();
                col = GetIndexFromN(left);
                symbol_stk.push(left);
                value_stk.push(la_stream[cur].first);
                goTo = Goto[row][col];
                if (goTo > 0) 
                    state_stk.push(goTo);
                else {
                    outputString += std::to_wstring(row);
                    outputString += L"和";
                    outputString += std::to_wstring(col);
                    outputString += L"查询Goto表失败\r\n";
                    return false;
                }
            }
            // error
            else {
                outputString += std::to_wstring(row);
                outputString += L"和";
                outputString += std::to_wstring(col);
                outputString += L"查询SLR表失败\r\n";
                return false;
            }
        }
        return false;
    }

    /* 先序遍历树 */
    void dfs(TreeNode* root, std::wstring& outputString) {
        if (!root) return;
        // 打印叶子节点
        if (root->children.empty()) {
            // 变量
            if (root->flag == 0)
                outputString += s2w(root->variable);
            // 常量
            else if (root->flag == 1)
                outputString += std::to_wstring(root->val);
            // 符号
            else if (root->flag == 2)
                outputString += s2w(root->fx);
        }
        for (auto i : root->children) dfs(i, outputString);
    }

    /* 打印表达式的值 */
    void print(std::wstring& outputString) {
        //TreeNode* t = root;
        dfs(this->root, outputString);
        outputString += L"\r\n";
    }
};