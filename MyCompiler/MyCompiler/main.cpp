#ifndef UNICODE
#define UNICODE
#endif 

#define _CRT_SECURE_NO_DEPRECATE


// 数组字符个数
const int N = 10010;

#include <windows.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <map>
#include <stack>
#include <unordered_map>

typedef std::pair<std::string, int> psi;

#include "LexicalAnalysis.h"
#include "SemanticAnalysis.h"


// 分割字符串
std::vector<std::string> split(const std::string& str, const std::string& delim) {
    std::vector<std::string> res;
    if (str.empty()) return res;
    // string -> char* 
    char* strs = new char[str.length() + 1];
    strcpy(strs, str.c_str());

    char* d = new char[delim.length() + 1];
    strcpy(d, delim.c_str());

    char* p = strtok(strs, d);
    while (p) {
        std::string s = p; // -> string
        res.push_back(s);
        p = strtok(NULL, d);
    }

    return res;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{

    // Register the window class.
    const wchar_t CLASS_NAME[] = L"MyClass";

    WNDCLASS wc = { };

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;

    RegisterClass(&wc);

    // Create the window.

    HWND hwnd = CreateWindowEx(
        0,                              // Optional window styles.
        CLASS_NAME,                     // Window class
        L"初等函数编译器",    // Window text
        WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,            // Window style

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
    );

    if (hwnd == NULL)
    {
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);

    // Run the message loop.

    MSG msg = { };
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    TCHAR prompt[] = L"在此处输入要分析的串（替换此行文本）";

    int wmId, wmEvent;

    static HFONT hFont;
    static HFONT hFontTitle;
    static HWND hwndInputEdit;
    static HWND hwndButton;
    static HWND hwndOutputEdit;
    static HWND hwndText1;
    static HWND hwndText2;

    TCHAR inputString[N];
    std::wstring outputString;

    // 词法分析器
    LexicalAnalysis la;
    // 语义分析器
    SemanticAnalysis sa;

    switch (uMsg)
    {
    case WM_CREATE:
    {
        // 字体
        hFont = CreateFont(-16, 0, 0, 0, FW_NORMAL,
            FALSE, FALSE, FALSE, DEFAULT_CHARSET,
            OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS, DEFAULT_QUALITY,
            FF_MODERN, TEXT("微软雅黑")
        );
        hFontTitle = CreateFont(-24, 0, 0, 0, FW_NORMAL,
            FALSE, FALSE, FALSE, DEFAULT_CHARSET,
            OUT_CHARACTER_PRECIS, CLIP_CHARACTER_PRECIS, DEFAULT_QUALITY,
            FF_MODERN, TEXT("微软雅黑")
        );

        // Title1
        hwndText1 = CreateWindow(
            L"static",
            L"需要编译的串",
            WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER | SS_CENTERIMAGE,
            20, 20, 600, 100,
            hwnd,
            NULL,
            (HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE),  //当前程序实例句柄
            NULL
        );

        // 输入编辑框
        hwndInputEdit = CreateWindowEx(
            0, L"EDIT",   // predefined class 
            NULL,         // no window title 
            WS_CHILD | WS_VISIBLE | WS_VSCROLL |
            ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL,
            20, 130, 600, 550,   // set size in WM_SIZE message 
            hwnd,         // parent window 
            NULL,   // edit control ID 
            (HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE),
            NULL        // pointer not needed 
        );
        // 输出提示文字
        SendMessage(hwndInputEdit, WM_SETTEXT, 0, (LPARAM)prompt);

        // 按钮
        hwndButton = CreateWindow(
            L"BUTTON",  // Predefined class; Unicode assumed 
            L"分析",      // Button text 
            WS_VISIBLE | WS_CHILD | BS_PUSHBUTTON,  // Styles 
            650,         // x position 
            320,         // y position 
            100,        // Button width
            50,        // Button height
            hwnd,     // Parent window
            (HMENU)1,       // menu.
            (HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE),
            NULL      // Pointer not needed.
        );

        // Title2
        hwndText2 = CreateWindow(
            L"static",
            L"分析结果",
            WS_CHILD | WS_VISIBLE | WS_BORDER | SS_CENTER | SS_CENTERIMAGE,
            800, 20, 600, 100,
            hwnd,
            NULL,
            (HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE),  //当前程序实例句柄
            NULL
        );

        // 输出编辑框
        hwndOutputEdit = CreateWindowEx(
            0, L"EDIT",   // predefined class 
            NULL,         // no window title 
            WS_CHILD | WS_VISIBLE | WS_VSCROLL |
            ES_LEFT | ES_MULTILINE | ES_AUTOVSCROLL,
            800, 130, 600, 550,   // set size in WM_SIZE message 
            hwnd,         // parent window 
            NULL,   // edit control ID 
            (HINSTANCE)GetWindowLongPtr(hwnd, GWLP_HINSTANCE),
            NULL        // pointer not needed 
        );

        // 设置控件字体
        SendMessage(hwndButton, WM_SETFONT, (WPARAM)hFont, NULL);
        SendMessage(hwndInputEdit, WM_SETFONT, (WPARAM)hFont, NULL);
        SendMessage(hwndOutputEdit, WM_SETFONT, (WPARAM)hFont, NULL);
        SendMessage(hwndText1, WM_SETFONT, (WPARAM)hFontTitle, NULL);
        SendMessage(hwndText2, WM_SETFONT, (WPARAM)hFontTitle, NULL);
    }
    break;
    case WM_COMMAND:
        wmId = LOWORD(wParam);
        wmEvent = HIWORD(wParam);
        switch (wmId) {
        case 1:
        {
            // 清空输出
            outputString.clear();

            //获取输入框的数据
            GetWindowText(hwndInputEdit, inputString, N);
            std::string ss;
            std::wstring wStr = inputString;
            ss = std::string(wStr.begin(), wStr.end());

            // 按行分割，Windows下换行符是\r\n
            std::vector<std::string> sss = split(ss, "\r\n");

            // 遍历每个串
            for (auto ss : sss) {
                outputString += L"当前串：";
                outputString += std::wstring(ss.begin(), ss.end());
                outputString += L"\r\n";

                // 检测结果
                bool la_ok, sa_ok;
                std::vector<psi> la_stream;

                // 词法分析
                la_ok = la.valid(ss, outputString);
                la_stream = la.get_result();
                outputString += L"词法分析结果：";
                la_ok ? outputString += L"合法" : outputString += L"非法";
                outputString += L"\r\n";

                if (la_ok) {
                    // 词法分析合法的进行语法分析
                    sa_ok = sa.check(la_stream, outputString);

                    outputString += L"语法分析结果：";
                    sa_ok ? outputString += L"合法" : outputString += L"非法";
                    outputString += L"\r\n";

                    // 如果语法分析合法才输出语义分析结果
                    if (sa_ok) {
                        outputString += L"语义分析结果：";
                        sa.print(outputString);
                    }
                }

                outputString += L"\r\n";
            }

            // 在另一个编辑框输出结果
            SendMessage(hwndOutputEdit, WM_SETTEXT, 0, (LPARAM)outputString.c_str());
        }
        break;
        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint(hwnd, &ps);

        FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW));

        EndPaint(hwnd, &ps);
    }
    return 0;

    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}